# Changes in Kiwa Link Obfuscator v0.1

## 0.1.3 2021-09-10

### Fixed

-   Fixed a decoding bug that caused links to have a wrong link text.