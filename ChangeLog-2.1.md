# Changes in Kiwa Link Obfuscator v2.1

## 2.1.0 2024-12-27

### Changed

-   Attributes can now be written as arrays. Writing them as strings has been deprecated and will be removed in v3.0. 