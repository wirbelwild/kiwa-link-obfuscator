/*!
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright (c) Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

/**
 * This file holds some basic functions which are needed by every link.
 * Per default, this code is included by and appended behind the first link which gets created,
 * but it is also possible to prevent this behaviour and use this code globally.
 * To prevent the include, you need to call `AbstractLink::disableAddingBasicJS()`. 
 */

/**
 * Decodes an URL in the way PHP has it encoded.
 * 
 * @param url
 * @returns {string}
 */
export const urlDecode = (url) => {
    return decodeURIComponent(
        url.replace(/\+/g, " ")
    );
};

/**
 * Runs the link action.
 * 
 * @param href
 */
export const action = (href) => {
    document.location.href = href;
};

/**
 * This function creates a functional link. It will
 * * decode whatever has been encoded
 * * wrap the @ with a span
 * * add some event listeners
 *
 * @param {HTMLAnchorElement} link
 */
export const handleLink = (link) => {
    let linkHref = link.dataset.href;
    let linkText = link.text || link.innerHTML;

    const isLinkTextEncrypted = "|||" === linkText.substring(0, 3);
    
    linkHref = urlDecode(linkHref);
    linkText = true === isLinkTextEncrypted ? urlDecode(linkText) : linkText;

    linkHref = encrypt(linkHref);
    linkText = isLinkTextEncrypted
        ? encrypt(linkText.substring(3, linkText.length))
        : linkText
    ;

    linkText = linkText.replace("@", "<span>@</span>");

    link.innerHTML = linkText;
    link.href = "javascript:void(0);";

    link.removeAttribute("data-id");
    link.removeAttribute("data-href");

    link.addEventListener("click", () => action(linkHref));
    link.addEventListener("touchdown", () => action(linkHref));
};