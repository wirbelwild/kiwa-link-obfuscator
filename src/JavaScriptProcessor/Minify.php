<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator\JavaScriptProcessor;

use Exception;
use JShrink\Minifier;
use MatthiasMullie\Minify\JS;

/**
 * The Minify class removes whitespaces, comments, and linebreaks.
 *
 * @see \Kiwa\LinkObfuscator\Tests\JavaScriptProcessor\MinifyTest
 */
class Minify implements JavaScriptProcessorInterface
{
    /**
     * Returns a processed string of JavaScript code.
     *
     * @param string $javascript The JavaScript code.
     * @return string
     */
    public function process(string $javascript): string
    {
        $javascript = (new JS($javascript))->minify();

        try {
            $javascript = (string) Minifier::minify(
                $javascript,
                [
                    'flaggedComments' => false,
                ]
            );
        } catch (Exception) {
        }
        
        return $javascript;
    }
}
