<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator\JavaScriptProcessor;

/**
 * The JavaScriptProcessorInterface defines the structure for every other JavaScript processor class.
 */
interface JavaScriptProcessorInterface
{
    /**
     * Returns a processed string of JavaScript code.
     *
     * @param string $javascript The JavaScript code.
     * @return string
     */
    public function process(string $javascript): string;
}
