<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator\JavaScriptProcessor;

/**
 * The Minify class returns the JavaScript code as it is.
 */
class Untouched implements JavaScriptProcessorInterface
{
    /**
     * Returns a processed string of JavaScript code.
     *
     * @param string $javascript The JavaScript code.
     * @return string
     */
    public function process(string $javascript): string
    {
        return $javascript;
    }
}
