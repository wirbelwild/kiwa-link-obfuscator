/*!
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright (c) Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

/**
 * Encrypts or decrypts a string by the Rot47 algorithm. This is needed to encode the PHP generated link.
 * Per default, this code is included by and appended behind every link which gets created,
 * but it is also possible to prevent this behaviour and use this code globally.
 * To prevent the include in every single link, you need to call `AbstractLink::disableAddingEncryptionJS()`.
 * 
 * @param input
 * @returns {string}
 */
export const encrypt = (input) => {
    const output = [];

    for (let counter = 0; counter < input.length; ++counter) {
        const charCode = input.charCodeAt(counter);

        if (charCode >= 33 && charCode <= 126) {
            output[counter] = String.fromCharCode(33 + ((charCode + 14) % 94));
            continue;
        }

        output[counter] = String.fromCharCode(charCode);
    }

    return output.join("");
};