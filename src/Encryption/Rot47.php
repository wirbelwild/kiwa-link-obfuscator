<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator\Encryption;

/**
 * The Rot47 class encrypts strings by the Rot47 algorithm.
 *
 * @see \Kiwa\LinkObfuscator\Tests\Encryption\Rot47Test
 */
class Rot47 implements EncryptionInterface
{
    /**
     * Returns an encrypted string.
     *
     * @param string $input The input which should get encrypted.
     * @return string
     */
    public function encrypt(string $input): string
    {
        return strtr(
            $input,
            '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~',
            'PQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNO'
        );
    }

    /**
     * Returns the JavaScript code, which can decrypt the encrypted string.
     *
     * @return string
     */
    public function getJavascript(): string
    {
        return (string) file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'Rot47.js');
    }
}
