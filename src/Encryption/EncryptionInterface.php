<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator\Encryption;

/**
 * The EncryptionInterface defines the structure for every other encryption class.
 */
interface EncryptionInterface
{
    /**
     * Returns an encrypted string.
     *
     * @param string $input The input which should get encrypted.
     * @return string
     */
    public function encrypt(string $input): string;

    /**
     * Returns the JavaScript code, which can decrypt the encrypted string.
     *
     * @return string
     */
    public function getJavascript(): string;
}
