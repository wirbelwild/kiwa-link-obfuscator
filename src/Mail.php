<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator;

/**
 * The Mail class is for creating an email link.
 *
 * @see \Kiwa\LinkObfuscator\Tests\MailTest
 */
class Mail extends AbstractLink
{
    protected string $action = 'mailto:';

    private bool $hasParameters = false;
    
    /**
     * Creates a new email link.
     *
     * @param string $mailHref Where the email link should point to; for example `hello@bitandblack.com`.
     * @param string|null $mailText A description of the email link; for example `Mail us`. If this one is empty, the email href will be used instead.
     * @param string|array<string, int|float|string|bool|null>|null $attributes Some attributes like class names or ids.
     * @param bool $encryptLinkText If the email link description should also be encrypted. This is true per default.
     */
    public function __construct(
        string $mailHref,
        string|null $mailText = null,
        string|array|null $attributes = null,
        bool $encryptLinkText = true
    ) {
        parent::__construct($mailHref, $mailText, $attributes, $encryptLinkText);
    }

    /**
     * Adds a parameter and encodes it.
     *
     * @param string $name
     * @param string $parameter
     * @return $this
     */
    private function addParameter(string $name, string $parameter): self
    {
        $separator = false === $this->hasParameters ? '?' : '&';
        $this->linkHref .= $separator . $name . '=' . rawurlencode($parameter);
        $this->hasParameters = true;
        return $this;
    }
    
    /**
     * Sets a subject to the Mail.
     *
     * @param string $subject
     * @return $this
     */
    public function setSubject(string $subject): self
    {
        return $this->addParameter('subject', $subject);
    }

    /**
     * Sets a body content to the Mail.
     *
     * @param string $body
     * @return $this
     */
    public function setMailBody(string $body): self
    {
        return $this->addParameter('body', $body);
    }
}
