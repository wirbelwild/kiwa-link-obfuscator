/*!
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright (c) Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

/**
 * This file contains the code which is needed by every link to detect and decrypt it.
 * Per default, this code is included by and appended behind every link which gets created,
 * but it is also possible to prevent this behaviour and use your own code.
 *
 * To prevent the include in every single link, you need to call `AbstractLink::disableAddingActionJS()`.
 * If you have this part globally, you can think about handling all links in one page,
 * for example, with the use of a forEach loop.
 *
 * Pay attention: This code has a selector with placeholder, which is filled by PHP.
 * Using this code outside PHP will __not__ work.
 */
handleLink(
    document.querySelector("a[data-id='%1$s']")
);