<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator;

use BitAndBlack\Helpers\StringHelper;
use Kiwa\LinkObfuscator\Encryption\EncryptionInterface;
use Kiwa\LinkObfuscator\Encryption\Rot47;
use Kiwa\LinkObfuscator\JavaScriptProcessor\JavaScriptProcessorInterface;
use Kiwa\LinkObfuscator\JavaScriptProcessor\Minify;
use Stringable;

/**
 * The AbstractLink class holds the logic for each link.
 *
 * @see \Kiwa\LinkObfuscator\Tests\AbstractLinkTest
 */
abstract class AbstractLink implements Stringable
{
    private string $html = '<a data-href="%1$s" data-id="%4$s"%3$s>%2$s</a>';
    
    protected string $action;

    private static ?EncryptionInterface $encryption = null;
    
    private static ?JavaScriptProcessorInterface $javaScriptProcessor = null;
    
    protected static int $uidCounter = 0;

    private static string $uidPrefix = '_klo';
    
    private static bool $addBasicJS = true;
    
    private static bool $addedBasicJS = false;

    private static bool $addEncryptionJS = true;

    private static bool $addedEncryptionJS = false;
    
    private static bool $addActionJS = true;
    
    /**
     * Creates a new link.
     *
     * @param string $linkHref Where the link should point to; an email address or a phone number.
     * @param string|null $linkText A description of the link.
     * @param string|array<string, int|float|string|bool|null>|null $attributes Some attributes like class names or ids.
     * @param bool $encryptLinkText If the link description should also be encrypted. This is true per default.
     */
    public function __construct(
        protected string $linkHref,
        protected string|null $linkText = null,
        protected string|array|null $attributes = null,
        protected bool $encryptLinkText = true,
    ) {
        /**
         * @todo Remove in v3.0.
         */
        if (true === is_string($this->attributes)) {
            trigger_error('Adding attributes as string has been deprecated. Please change it to a named array.', E_USER_DEPRECATED);
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getLink();
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        if (null === self::$encryption) {
            self::$encryption = new Rot47();
        }

        if (null === self::$javaScriptProcessor) {
            self::$javaScriptProcessor = new Minify();
        }

        $mailHrefEncrypted = urlencode(self::$encryption->encrypt($this->action . $this->linkHref));

        $linkText = $this->linkText ?? $this->linkHref;

        if (true === $this->encryptLinkText) {
            $linkText = self::$encryption->encrypt($linkText);
            $linkText = '|||' . urlencode($linkText);
        }

        $attributes = null;

        if (true === is_array($this->attributes)) {
            $attributes = array_map(
                static fn (string $attributeName, int|float|string|bool|null $attributeValue): string => $attributeName . '="' . StringHelper::booleanToString($attributeValue) . '"',
                array_keys($this->attributes),
                array_values($this->attributes)
            );
            $attributes = ' ' . implode(' ', $attributes);
        }

        if (true === is_string($this->attributes)) {
            $attributes = ' ' . $this->attributes;
        }

        $uid = $this->getNewUID();

        $htmlOutput = sprintf(
            $this->html,
            $mailHrefEncrypted,
            $linkText,
            $attributes,
            $uid
        );

        $basicJS = '';

        if (true === self::$addBasicJS && false === self::$addedBasicJS) {
            $basicJS = (string) file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'Basic.js');
            $basicJS = str_replace('export const', 'const', $basicJS);
            self::$addedBasicJS = true;
        }

        $encryptionJS = '';

        if (true === self::$addEncryptionJS && false === self::$addedEncryptionJS) {
            $encryptionJS = self::$encryption->getJavascript();
            $encryptionJS = str_replace('export const', 'const', $encryptionJS);
            self::$addedEncryptionJS = true;
        }

        $actionJS = '';

        if (true === self::$addActionJS) {
            $actionJS = (string) file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'Action.js');
            $actionJS = sprintf(
                $actionJS,
                $uid
            );
        }

        $javascriptOutput = '';

        if (true === self::$addBasicJS || true === self::$addEncryptionJS || true === self::$addActionJS) {
            $javascriptOutput = $basicJS . PHP_EOL . $encryptionJS . PHP_EOL . $actionJS;
            $javascriptOutput = self::$javaScriptProcessor->process($javascriptOutput);
            $javascriptOutput = str_replace(PHP_EOL, '', $javascriptOutput);
            $javascriptOutput = '<script>' . $javascriptOutput . '</script>';
        }

        $link = $htmlOutput . $javascriptOutput;

        ++self::$uidCounter;
        
        return $link;
    }

    /**
     * Returns a new unique id.
     *
     * @return string
     */
    private function getNewUID(): string
    {
        return self::$uidPrefix . self::$uidCounter;
    }

    /**
     * Sets the encryption. This is Rot47 per default.
     *
     * @param EncryptionInterface $encryption
     */
    public static function setEncryption(EncryptionInterface $encryption): void
    {
        self::$encryption = $encryption;
    }

    /**
     * Sets a javascript processor. Per default, `Minify` will be used.
     *
     * @param JavaScriptProcessorInterface $javaScriptProcessor
     */
    public static function setJavaScriptProcessor(JavaScriptProcessorInterface $javaScriptProcessor): void
    {
        self::$javaScriptProcessor = $javaScriptProcessor;
    }
    
    /**
     * Sets the prefix for the unique ids. This is `_klo` per default.
     *
     * @param string $uidPrefix
     */
    public static function setUidPrefix(string $uidPrefix): void
    {
        self::$uidPrefix = $uidPrefix;
    }

    /**
     * Disabled the inclusion of the Basic.js file in every link.
     * If this behaviour is disabled, you need to include the code by yourself
     * in your own JavaScript code.
     */
    public static function disableAddingBasicJS(): void
    {
        self::$addBasicJS = false;
    }

    /**
     * Disabled the inclusion of the encryption js file in every link.
     * If this behaviour is disabled, you need to include the code by yourself
     * in your own JavaScript code.
     */
    public static function disableAddingEncryptionJS(): void
    {
        self::$addEncryptionJS = false;
    }

    /**
     * Disabled the inclusion of the Action.js file in every link.
     * If this behaviour is disabled, you need to include the code by yourself
     * in your own JavaScript code.
     */
    public static function disableAddingActionJS(): void
    {
        self::$addActionJS = false;
    }

    /**
     * Returns the unencrypted link href. This can be used for debugging or in special cases,
     * where the raw href is needed.
     *
     * @return string
     */
    public function getLinkHref(): string
    {
        return $this->linkHref;
    }
}
