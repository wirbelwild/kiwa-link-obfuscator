<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator;

/**
 * The SMS class is for creating an SMS link.
 *
 * @see \Kiwa\LinkObfuscator\Tests\SMSTest
 */
class SMS extends AbstractLink
{
    protected string $action = 'sms:';

    /**
     * Creates a new SMS link.
     *
     * @param string $smsHref Where the SMS should point to; for example `004971199528660`.
     * @param string|null $smsText A description of the SMS link; for example `+49.711.995286-60`. If this one is empty, the SMS href will be used instead.
     * @param string|array<string, int|float|string|bool|null>|null $attributes Some attributes like class names or ids.
     * @param bool $encryptLinkText If the SMS link description should also be encrypted. This is true per default.
     */
    public function __construct(
        string $smsHref,
        string|null $smsText = null,
        string|array|null $attributes = null,
        bool $encryptLinkText = true
    ) {
        parent::__construct($smsHref, $smsText, $attributes, $encryptLinkText);
    }

    /**
     * Sets a body content to the SMS.
     *
     * @param string $body
     * @return SMS
     */
    public function setSMSBody(string $body): self
    {
        $this->linkHref .= '?&' . $body;
        return $this;
    }
}
