<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator;

/**
 * The Phone class is for creating a phone link.
 *
 * @see \Kiwa\LinkObfuscator\Tests\PhoneTest
 */
class Phone extends AbstractLink
{
    protected string $action = 'tel:';
    
    /**
     * Creates a new phone link.
     *
     * @param string $phoneHref Where the phone link should point to; for example `004971199528660`.
     * @param string|null $phoneText A description of the phone link; for example `+49.711.995286-60`. If this one is empty, the phone href will be used instead.
     * @param string|array<string, int|float|string|bool|null>|null $attributes Some attributes like class names or ids.
     * @param bool $encryptLinkText If the phone link description should also be encrypted. This is true per default.
     */
    public function __construct(
        string $phoneHref,
        string|null $phoneText = null,
        string|array|null $attributes = null,
        bool $encryptLinkText = true
    ) {
        parent::__construct($phoneHref, $phoneText, $attributes, $encryptLinkText);
    }
}
