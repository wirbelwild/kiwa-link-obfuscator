# Changes in Kiwa Link Obfuscator v0.2

## 0.2.1 2021-11-22

## Fixed 

-   Fixed a bug with HTML as anchor child.

## 0.2.0 2021-09-10

### Changed

-   Link texts that don't have an encryption are now without encoding too. 