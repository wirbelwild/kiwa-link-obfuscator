[![PHP from Packagist](https://img.shields.io/packagist/php-v/kiwa/link-obfuscator)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/kiwa/link-obfuscator/v/stable)](https://packagist.org/packages/kiwa/link-obfuscator)
[![Total Downloads](https://poser.pugx.org/kiwa/link-obfuscator/downloads)](https://packagist.org/packages/kiwa/link-obfuscator)
[![License](https://poser.pugx.org/kiwa/link-obfuscator/license)](https://packagist.org/packages/kiwa/link-obfuscator)

<p align="center">
    <a href="https://www.kiwa.io" target="_blank">
        <img src="https://www.kiwa.io/build/images/kiwa-logo.png" alt="Kiwa Logo" width="150">
    </a>
</p>

# Kiwa Link Obfuscator

Strong link obfuscation with PHP and JavaScript. Object-oriented, using the Rot47 algorithm. Hides email, phone and sms links from being crawled by spambots.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/kiwa/link-obfuscator). Add it to your project by running `$ composer require kiwa/link-obfuscator`. 

## Usage

There are three classes available: `Mail`, `Phone` and `SMS`. All of them can be used in the same way: 

```php
<?php

use Kiwa\LinkObfuscator\Mail;

echo new Mail('hello@bitandblack.com');

// Basically equal to <a href="mailto:hello@bitandblack.com">hello@bitandblack.com</a>
```

The output will be: `<a data-href="%3E2%3A%3DE%40i96%3D%3D%40o3%3AE2%3F53%3D24%3C%5D4%40%3E" data-id="_klo0">|||96%3D%3D%40o3%3AE2%3F53%3D24%3C%5D4%40%3E</a>`, followed by some JavaScript code.

There are some parameters available:

1.  The first one is the link href
2.  Second one is the link description
3.  The third can be every additional parts like class names
4.  If the link description should also be encrypted.

Using them, the example above would look like:

```php
<?php

use Kiwa\LinkObfuscator\Mail;

echo new Mail('hello@bitandblack.com', 'Mail us', ['class' => 'btn'], false);

// Basically equal to <a href="mailto:hello@bitandblack.com" class="btn">Mail us</a>
```

Now the output is: `<a data-href="%3E2%3A%3DE%40i96%3D%3D%40o3%3AE2%3F53%3D24%3C%5D4%40%3E" data-id="_klo0" class="btn">Mail us</a>`, also followed by some JavaScript code.

The JavaScript will then decrypt the link. But instead of appending raw data into the page, the link href will be stored in some event listeners. So even if the page will be parsed with active JavaScript, the link will simply look like `<a href="javascript:void(0);">hello<span>@</span>bitandblack.com</a>`.

### Options

#### Encryption 

The Kiwa Link Obfuscator uses the Rot47 algorithm per default. You can change this by adding a custom algorithm using `AbstractLink::setEncryption(new MyCustomEncryption())`. Please note that the algorithm needs also to be available in JavaScript.

#### UID

The default prefix for all link ids is `_klo`. This id is stored as `data-id` to keep the `id` free for your usage. To change the prefix you can call `AbstractLink::setUidPrefix('myCustomPrefix')`.

#### JavaScript 

Every html link will have its own JavaScript code at the end. The first of them has also the functions which are needed to create functional links. If you have a lot of links in one page, want to keep your page DRY, or whatever else, you can disable this functionality and use the code in your own way. 

There are three parts of JavaScript code, which can be disabled: 

```php
<?php

use Kiwa\LinkObfuscator\AbstractLink;

AbstractLink::disableAddingActionJS();
AbstractLink::disableAddingBasicJS();
AbstractLink::disableAddingEncryptionJS();
```

Now you can add that code to your JavaScript file, for example by writing

````javascript
import { handleLink } from "/vendor/kiwa/link-obfuscator/src/Basic";
import { encrypt } from "/vendor/kiwa/link-obfuscator/src/Encryption/Rot47";
global.encrypt = encrypt;
````

Now a custom code is needed as replacement for the `Action.js`. This could be:

````javascript
const links = document.querySelectorAll("a[data-id^='_klo']");

links.forEach((link) => handleLink(link));
````

## Help 

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).
