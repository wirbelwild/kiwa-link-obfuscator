<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator\Tests;

use Kiwa\LinkObfuscator\SMS;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class SMSTest.
 */
class SMSTest extends TestCase
{
    #[RunInSeparateProcess]
    public function testCanCreateLink(): void
    {
        $sms = (string) new SMS('00491636674146');
        
        self::assertStringContainsString(
            'D%3EDi__ch%60ebeefc%60ce',
            $sms
        );

        self::assertStringContainsString(
            '|||__ch%60ebeefc%60ce',
            $sms
        );
    }

    public function testCanAddContent(): void
    {
        $sms = new SMS('00491636674146', 'Write a SMS');
        $sms->setSMSBody('My SMS body');

        self::assertSame(
            '00491636674146?&My SMS body',
            $sms->getLinkHref()
        );

        self::assertStringContainsString(
            'D%3EDi__ch%60ebeefc%60cenU%7CJ+%24%7C%24+3%405J',
            (string) $sms
        );
    }

    public function testCanHandleEncryptedLinkText(): void
    {
        self::assertStringContainsString(
            '|||Zch+%5D+W_X%60eb+%5D+eef+c%60+ce',
            (string) new SMS('hello@bitandblack.com', '+49 . (0)163 . 667 41 46', null, true)
        );

        self::assertStringContainsString(
            '+49 . (0)163 . 667 41 46',
            (string) new SMS('00491636674146', '+49 . (0)163 . 667 41 46', null, false)
        );
    }
}
