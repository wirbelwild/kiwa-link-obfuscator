<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator\Tests\JavaScriptProcessor;

use Kiwa\LinkObfuscator\JavaScriptProcessor\Minify;
use PHPUnit\Framework\TestCase;

/**
 * Class MinifyTest
 */
class MinifyTest extends TestCase
{
    public function testCanMinify(): void
    {
        $javaScript = (string) file_get_contents(
            dirname(__FILE__, 3) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Basic.js'
        );

        self::assertStringContainsString(
            PHP_EOL,
            $javaScript
        );

        self::assertStringContainsString(
            '@copyright Copyright (c)',
            $javaScript
        );

        $minify = new Minify();
        $javaScript = $minify->process($javaScript);
        
        self::assertStringNotContainsString(
            PHP_EOL,
            $javaScript
        );

        self::assertStringNotContainsString(
            '@copyright Copyright (c)',
            $javaScript
        );
    }
}
