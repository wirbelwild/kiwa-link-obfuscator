<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator\Tests;

use Kiwa\LinkObfuscator\AbstractLink;
use Kiwa\LinkObfuscator\Mail;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class AbstractLinkTest.
 */
class AbstractLinkTest extends TestCase
{
    #[RunInSeparateProcess]
    public function testSetUidPrefix(): void
    {
        AbstractLink::setUidPrefix('#test');
        $mail = (string) new Mail('test');
        
        self::assertStringContainsString(
            '#test',
            $mail
        );
    }

    #[RunInSeparateProcess]
    public function testCanPaginate(): void
    {
        $mail1 = (string) new Mail('hello1@bitandblack.com');
        $mail2 = (string) new Mail('hello2@bitandblack.com');

        self::assertStringContainsString(
            'data-id="_klo0"',
            $mail1
        );

        self::assertStringContainsString(
            'data-id="_klo1"',
            $mail2
        );
    }
    
    #[RunInSeparateProcess]
    public function testCanDisableAllJavaScript(): void
    {
        AbstractLink::disableAddingActionJS();
        AbstractLink::disableAddingBasicJS();
        AbstractLink::disableAddingEncryptionJS();

        $mail = (string) new Mail('test');

        self::assertStringNotContainsString(
            '<script>',
            $mail
        );
    }
    #[RunInSeparateProcess]
    public function testCanRemoveExports(): void
    {
        $mail = (string) new Mail('test');

        self::assertStringNotContainsString(
            'export const action',
            $mail
        );
        
        self::assertStringNotContainsString(
            'export const encrypt',
            $mail
        );
    }

    #[RunInSeparateProcess]
    public function testAddsJavaScriptFunctionsOnlyOnce(): void
    {
        $mails =
            new Mail('hello1@bitandblack.com') .
            PHP_EOL .
            new Mail('hello2@bitandblack.com')
        ;
        
        $urlDecode = substr_count($mails, 'const urlDecode');

        self::assertSame(
            1,
            $urlDecode
        );

        $handleLink = substr_count($mails, 'const handleLink');

        self::assertSame(
            1,
            $handleLink
        );

        $encrypt = substr_count($mails, 'const encrypt');

        self::assertSame(
            1,
            $encrypt
        );
    }

    /**
     * @todo Remove in v3.0.
     * @return void
     */
    public function testCanUseAttributesAsString(): void
    {
        $this->expectUserDeprecationMessage('Adding attributes as string has been deprecated. Please change it to a named array.');

        $mail = (string) new Mail('test', attributes: 'class="button" id="menu" data-boolean="false"');

        self::assertStringContainsString(
            'class="button" id="menu" data-boolean="false"',
            $mail
        );
    }

    public function testCanUseAttributesAsArray(): void
    {
        $mail = (string) new Mail('test', attributes: [
            'class' => 'button',
            'id' => 'menu',
            'data-boolean' => false,
        ]);

        self::assertStringContainsString(
            'class="button" id="menu" data-boolean="false"',
            $mail
        );
    }
}
