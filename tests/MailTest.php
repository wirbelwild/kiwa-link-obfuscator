<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator\Tests;

use Kiwa\LinkObfuscator\Mail;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class MailTest.
 */
class MailTest extends TestCase
{
    #[RunInSeparateProcess]
    public function testCanCreateLink(): void
    {
        $mail = (string) new Mail('hello@bitandblack.com');

        self::assertStringContainsString(
            '%3E2%3A%3DE%40i96%3D%3D%40o3%3AE2%3F53%3D24%3C%5D4%40%3E',
            $mail
        );

        self::assertStringContainsString(
            '|||96%3D%3D%40o3%3AE2%3F53%3D24%3C%5D4%40%3E',
            $mail
        );

        self::assertStringContainsString(
            'const encrypt',
            $mail
        );

        self::assertStringContainsString(
            'const urlDecode',
            $mail
        );
        
        self::assertStringContainsString(
            'data-id="_klo0"',
            $mail
        );

        self::assertStringContainsString(
            'document.querySelector("a[data-id=\'_klo0\']")',
            $mail
        );
    }
    
    public function testCanAddContent(): void
    {
        $mail = new Mail('test@example.com', 'Write a mail');
        $mail
            ->setSubject('My subject')
            ->setMailBody('My body ' . PHP_EOL . 'next line')
        ;
        
        self::assertSame(
            'test@example.com?subject=My%20subject&body=My%20body%20%0Anext%20line',
            $mail->getLinkHref()
        );
        
        self::assertStringContainsString(
            '%3E2%3A%3DE%40iE6DEo6I2%3EA%3D6%5D4%40%3EnDF3%3B64El%7CJTa_DF3%3B64EU3%405Jl%7CJTa_3%405JTa_T_p%3F6IETa_%3D%3A%3F6',
            (string) $mail
        );
    }

    public function testCanHandleEncryptedLinkText(): void
    {
        self::assertStringContainsString(
            '|||96%3D%3D%40o3%3AE2%3F53%3D24%3C%5D4%40%3E',
            (string) new Mail('hello@bitandblack.com', 'hello@bitandblack.com', null, true)
        );

        self::assertStringContainsString(
            'hello@bitandblack.com',
            (string) new Mail('hello@bitandblack.com', 'hello@bitandblack.com', null, false)
        );
    }
}
