<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator\Tests;

use Kiwa\LinkObfuscator\Phone;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;

/**
 * Class PhoneTest.
 */
class PhoneTest extends TestCase
{
    #[RunInSeparateProcess]
    public function testCanCreateLink(): void
    {
        $phone = (string) new Phone('004971199528668');
        
        self::assertStringContainsString(
            'E6%3Di__chf%60%60hhdageeg',
            $phone
        );

        self::assertStringContainsString(
            '|||__chf%60%60hhdageeg',
            $phone
        );
    }
    
    public function testCanHandleEncryptedLinkText(): void
    {
        self::assertStringContainsString(
            '|||Zch+%5D+W_Xf%60%60+%5D+hh+da+ge%5Ce_',
            (string) new Phone('004971199528668', '+49 . (0)711 . 99 52 86-60', null, true)
        );
        
        self::assertStringContainsString(
            '+49 . (0)711 . 99 52 86-60',
            (string) new Phone('004971199528668', '+49 . (0)711 . 99 52 86-60', null, false)
        );
    }
}
