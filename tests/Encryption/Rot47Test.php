<?php

/**
 * Kiwa Link Obfuscator.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\LinkObfuscator\Tests\Encryption;

use Kiwa\LinkObfuscator\Encryption\Rot47;
use PHPUnit\Framework\TestCase;

/**
 * Class Rot47Test.
 */
class Rot47Test extends TestCase
{
    public function testCanEncrypt(): void
    {
        $rot47 = new Rot47();
        $result = $rot47->encrypt('Hello World!');

        self::assertSame(
            'w6==@ (@C=5P',
            $result
        );
        
        $result = $rot47->encrypt($result);

        self::assertSame(
            'Hello World!',
            $result
        );
    }
}
