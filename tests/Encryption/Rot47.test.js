import { encrypt } from "../../src/Encryption/Rot47";
import { expect, test } from "@jest/globals";

test("Can encrypt Rot47", () => {
    expect(encrypt("Hello World!")).toBe("w6==@ (@C=5P");
});

test("Can decrypt Rot47", () => {
    expect(encrypt("w6==@ (@C=5P")).toBe("Hello World!");
});