import { urlDecode, handleLink } from "../src/Basic";
import { encrypt } from "../src/Encryption/Rot47";
import { expect, test } from "@jest/globals";
global.encrypt = encrypt;

test("Can decode PHP encoded string", () => {
    expect(urlDecode("Hello+World%21")).toBe("Hello World!");
    expect(urlDecode("%2B49+.+%280%29711+.+99+52+86-60")).toBe("+49 . (0)711 . 99 52 86-60");
});

test("Can create link", () => {
    document.body.innerHTML = `<a data-href="%3E2%3A%3DE%40i96%3D%3D%40o3%3AE2%3F53%3D24%3C%5D4%40%3E" data-id="_klo0">|||96%3D%3D%40o3%3AE2%3F53%3D24%3C%5D4%40%3E</a>`;
    const link = document.querySelector("a[data-id='_klo0']");
    expect(typeof link).toBe("object");
    handleLink(link);
    expect(link.innerHTML).toBe("hello<span>@</span>bitandblack.com");
    expect(link.href).toBe("javascript:void(0);");
});

test("Can create link without HTML", () => {
    document.body.innerHTML = `<a data-href="%3E2%3A%3DE%40i96%3D%3D%40o3%3AE2%3F53%3D24%3C%5D4%40%3E" data-id="_klo0">E-Mail schreiben</a>`;
    const link = document.querySelector("a[data-id='_klo0']");
    expect(typeof link).toBe("object");
    handleLink(link);
    expect(link.innerHTML).toBe("E-Mail schreiben");
    expect(link.href).toBe("javascript:void(0);");
});

test("Can create link with HTML", () => {
    document.body.innerHTML = `<a data-href="%3E2%3A%3DE%40i96%3D%3D%40o3%3AE2%3F53%3D24%3C%5D4%40%3E" data-id="_klo0"><i class="fas fa-envelope"></i></a>`;
    const link = document.querySelector("a[data-id='_klo0']");
    expect(typeof link).toBe("object");
    handleLink(link);
    expect(link.innerHTML).toBe("<i class=\"fas fa-envelope\"></i>");
    expect(link.href).toBe("javascript:void(0);");
});